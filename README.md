# Exads Test

- Observations
I focused on solving the problems through the simplest way possible;
How are simple exercises, I decided to don't use any framework or ORM;
So I made everything with "pure PHP", except for the autoload that I used composer;
I avoided creating some tests classes because it's hard to check outputs like: texts or random numbers;

If it's necessary, I could make other applications using all that I haven't used here:
Framework, ORM, Tests, etc... or any other that you ask for!

# Application

- Configuration
Before running the application, please follow the three steps bellow:

1. Run "composer install" in the project folder

2. Execute the following SQL commands in your database:

    CREATE SCHEMA `exads` ;

    CREATE TABLE `exads`.`exads_test` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `age` INT(11) NOT NULL,
    `job_title` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`));

    CREATE TABLE `exads`.`ab_test` (
    `design_id` INT(11) NOT NULL AUTO_INCREMENT,
    `design_name` VARCHAR(20) NOT NULL,
    `split_percent` INT(11) NOT NULL,
    PRIMARY KEY (`design_id`));

    INSERT INTO `exads`.`ab_test` (`design_id`, `design_name`, `split_percent`) VALUES ('1', 'Design 1', '50');
    INSERT INTO `exads`.`ab_test` (`design_name`, `split_percent`) VALUES ('Design 2', '25');
    INSERT INTO `exads`.`ab_test` (`design_name`, `split_percent`) VALUES ('Design 3', '25');

3. Set your database configurations in the file /app/config/db.php

- Application
To run the application, access the project URL and fill the exercise number in the 'exercise' param
Examples in my localhost:
http://localhost/exads?exercise=1
http://localhost/exads?exercise=2