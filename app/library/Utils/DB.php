<?php
namespace Library\Utils;

use \PDO;

class DB extends PDO
{

    /**
     * 
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param array $options
     */
    public function __construct(string $dsn = '', string $username = null, string $password = null, array $options = null)
    {
        $params = require("../app/config/db.php");
        parent::__construct("mysql:host=" . $params['host'] . ";dbname=" . $params['dbname'], $params['user'], $params['password']);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Execute a query
     * 
     * @param string $query
     * @return array
     */
    public function select(string $query): array
    {
        $rows = $this->query($query)->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
}
