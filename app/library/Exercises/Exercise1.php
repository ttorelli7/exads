<?php
namespace Library\Exercises;

class Exercise1 implements ExerciseInterface
{

    /**
     * I focused on making it in the simplest way with the minimum of code possible
     */
    public function run()
    {
        for ($i = 1; $i <= 100; $i ++) {
            $label = ($i % 3 == 0 ? "Fizz" : "") . ($i % 5 == 0 ? "Buzz" : "");
            echo ($label ? $label : $i) . "<br/>";
        }
    }
}
