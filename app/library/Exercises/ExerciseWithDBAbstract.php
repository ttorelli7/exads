<?php
namespace Library\Exercises;

use Library\Utils\DB;

abstract class ExerciseWithDBAbstract
{

    /**
     *
     * @var DB
     */
    protected $db;

    /**
     * Return DB connection
     * 
     * @return DB
     */
    protected function getDB(): DB
    {
        if (!$this->db) {
            $this->db = new DB();
        }
        return $this->db;
    }
}
