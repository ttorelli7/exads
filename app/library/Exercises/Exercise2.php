<?php
namespace Library\Exercises;

class Exercise2 implements ExerciseInterface
{

    /**
     * Firstly I select 500 random numbers
     * Then I select a random position from the size of the array of numbers (0 to 499)
     * Then I write the selected position / the selected value
     * And finally I remove this position from the array of numbers
     */
    public function run()
    {
        $max = 500;
        $numbers = [];
        for ($i = 0; $i < $max; $i ++) {
            $numbers[] = rand(1, $max);
        }
        $randPos = rand(0, $max - 1);
        echo "Missing element at position: " . $randPos . " (old value: " . $numbers[$randPos] . ")";
        unset($numbers[$randPos]);
    }
}
