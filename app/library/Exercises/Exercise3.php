<?php
namespace Library\Exercises;

use Library\Utils\DB;
use Library\Exercises\ExerciseWithDBAbstract;

class Exercise3 extends ExerciseWithDBAbstract implements ExerciseInterface
{
    /*
      CREATE SCHEMA `exads` ;

      CREATE TABLE `exads`.`exads_test` (
      `id` INT(11) NOT NULL AUTO_INCREMENT,
      `name` VARCHAR(50) NOT NULL,
      `age` INT(11) NOT NULL,
      `job_title` VARCHAR(50) NOT NULL,
      PRIMARY KEY (`id`));
     */

    /**
     * I've used PDO to ensure the safety of the application
     * We all know that now a days the most known ORMs make this automatic,
     * But I think it's good to know the best way to do it without ORMs too, like I did here
     * In the example below I sanitize the entries to avoid SQL injection
     */
    public function run()
    {
        $this->insert("Example name'); DROP TABLE exads_test; --", rand(18, 70), "Example job title");
        $rows = $this->getDB()->select("SELECT name, age, job_title FROM exads_test");
        var_dump($rows);
    }

    /**
     * Insert a record into the table
     * 
     * @return \self
     */
    private function insert($name, $age, $jobTitle): self
    {
        $sql = 'INSERT INTO exads_test (name, age, job_title) VALUES (:name, :age, :job_title)';
        $sth = $this->getDB()->prepare($sql);

        $sth->bindParam(':name', $name, DB::PARAM_STR);
        $sth->bindParam(':age', $age, DB::PARAM_INT);
        $sth->bindParam(':job_title', $jobTitle, DB::PARAM_STR);

        $sth->execute();
        return $this;
    }
}
