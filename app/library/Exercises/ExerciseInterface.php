<?php
namespace Library\Exercises;

interface ExerciseInterface
{

    /**
     * Method to run the exercise
     */
    public function run();
}
