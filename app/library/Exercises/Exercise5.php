<?php
namespace Library\Exercises;

use Library\Exercises\ExerciseWithDBAbstract;

class Exercise5 extends ExerciseWithDBAbstract implements ExerciseInterface
{
    /*
      CREATE SCHEMA `exads` ;

      CREATE TABLE `exads`.`ab_test` (
      `design_id` INT(11) NOT NULL AUTO_INCREMENT,
      `design_name` VARCHAR(20) NOT NULL,
      `split_percent` INT(11) NOT NULL,
      PRIMARY KEY (`design_id`));

      INSERT INTO `exads`.`ab_test` (`design_id`, `design_name`, `split_percent`) VALUES ('1', 'Design 1', '50');
      INSERT INTO `exads`.`ab_test` (`design_name`, `split_percent`) VALUES ('Design 2', '25');
      INSERT INTO `exads`.`ab_test` (`design_name`, `split_percent`) VALUES ('Design 3', '25');

     */

    /**
     * Firstly I select the values from the table "ab_test" and check if the sum of the column "split_percent" is 100%
     * Then I select randomly a number between 1 and 100
     * And finally I check through a loop adding the percent values, which design is selected with this number
     */
    public function run()
    {
        $rows = $this->getDB()->select("SELECT * FROM ab_test");
        $this->validate($rows);
        $row = $this->rand($rows);
        echo "Randomly selected '" . $row['design_name'] . "'";
    }

    /**
     * Validate if the sum of the column "split_percent" is 100%
     * 
     * @param array $rows
     * @return \self
     * @throws \Exception
     */
    private function validate(array $rows): self
    {
        $percentSum = 0;
        foreach ($rows as $row) {
            $percentSum += $row['split_percent'];
        }
        if ($percentSum != 100) {
            throw new \Exception("The sum of column 'split_percent' needs to be 100%, it currently is " . $percentSum . "%");
        }
        return $this;
    }

    /**
     * Select a random design
     * 
     * @param array $rows
     * @return array
     */
    private function rand(array $rows): array
    {
        $percent = rand(1, 100);
        $percentSum = 0;
        foreach ($rows as $row) {
            $percentSum += $row['split_percent'];
            if ($percent <= $percentSum) {
                break;
            }
        }
        return $row;
    }
}
