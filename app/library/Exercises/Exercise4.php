<?php
namespace Library\Exercises;

use DateTime;

class Exercise4 implements ExerciseInterface
{

    /**
     *
     * @var DateTime
     */
    private $dateTime;

    public function __construct()
    {
        $this->dateTime = new DateTime();
    }

    /**
     * Sets the date and time for the calculation
     * 
     * @param DateTime $dateTime
     * @return \self
     */
    public function setDateTime(DateTime $dateTime): self
    {
        $this->dateTime = $dateTime;
        return $this;
    }

    /**
     * Firstly I identify the next wednesday and saturday
     * Then I check the difference between the timestamps with the current date/time
     * And finally I identify which one is the closest draw date available (wednesday or saturday)
     * 
     * You can set the specific date/time to compare calling the method "setDateTime" as in the examples bellow
     */
    public function run()
    {
        //$this->setDateTime(new \DateTime("2019-02-16 19:59:59"));
        //$this->setDateTime(new \DateTime("2019-02-16 20:00:01"));
        $drawTime = '20:00:00';

        $wednesday = clone $this->dateTime;
        $goToNextWednesday = $wednesday->format('w') != 3 || strtotime($wednesday->format('H:i:s')) > strtotime($drawTime);
        $wednesday->modify(($goToNextWednesday ? 'next wednesday ' : '') . $drawTime);

        $saturday = clone $this->dateTime;
        $goToNextSaturday = $saturday->format('w') != 6 || strtotime($saturday->format('H:i:s')) > strtotime($drawTime);
        $saturday->modify(($goToNextSaturday ? 'next saturday ' : '') . $drawTime);

        $intervalToWednesday = $wednesday->getTimestamp() - $this->dateTime->getTimestamp();
        $intervalToSaturday = $saturday->getTimestamp() - $this->dateTime->getTimestamp();

        $nextDate = ($intervalToWednesday < $intervalToSaturday) ? $wednesday : $saturday;
        echo "Next valid draw date based on " . $this->dateTime->format('l d/m/Y H:i:s') . " is <b>" . $nextDate->format("l d/m/Y H:i:s") . "</b>";
    }
}
