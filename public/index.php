<?php
try {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $autoloadFile = "../vendor/autoload.php";
    if (!file_exists($autoloadFile)) {
        throw new \Exception("Please run the composer install!");
    }
    require_once($autoloadFile);

    if (!isset($_GET['exercise'])) {
        throw new \Exception("Please inform the 'exercise' param!");
    }
    $exerciseNumber = (int) $_GET['exercise'];
    $className = '\Library\Exercises\Exercise' . $exerciseNumber;
    if (!class_exists($className)) {
        throw new \Exception("Exercise " . $exerciseNumber . " not found!");
    }
    $exercise = new $className;
    $exercise->run();
} catch (\Exception $e) {
    echo $e->getMessage();
}
